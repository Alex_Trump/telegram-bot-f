<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once('vendor/autoload.php');
require_once('settings/config.php');

$result = Telegram\Api\Curl::request(
    'setWebhook', 
    ['url' => $_SERVER['SERVER_NAME'] . Telegram\Config::get('folder') .'/index.php'],
    Telegram\Config::get('api_key')
);

echo "<pre>";
print_r($result);
echo "</pre>";

?>
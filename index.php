<?php

header('Content-type: text/html; charset=utf-8');

echo "ok";

ini_set('log_errors', 'On');
ini_set('error_log', 'php_errors.log');

$tg_data = json_decode(file_get_contents('php://input'), true);

if (!isset($tg_data['update_id']))
    exit();

require_once('vendor/autoload.php');
require_once('settings/config.php');
require_once('settings/routes.php');

set_exception_handler(function ($exception) {
    echo "ok";
    \Telegram\Api\Send::message(319086997, $exception->getMessage());
});

$tg = new Telegram\Webhook($tg_data); //создаем объект с данными от телеги

Telegram\DB::connect(); //подключаем базу

//если команда стартовая, то проводим стартовые операции
if ($tg->callback_status === false && $tg->checkStart() === true)
    Telegram\Start::startOperations($tg);

//получаем маршрут по названию
$route = Telegram\Route::get($tg->name_command);


if (isset($route['no_act']) && $route['no_act'] === true)
    Telegram\Api\Sequence::setCommandActNull($tg->user['id']);

//Проверяем или у нас не запущена активная команда
$command_act = Telegram\Api\Sequence::getCommandAct($tg->user['id']);

//иногда даже при запущенном активном режиме надо разрешать нажатие на кнопку
//тут идет проверка
if ($command_act !== null && !isset($route['no_act'])) {
    $tg->name_command = $command_act;
    $route = Telegram\Route::get($tg->name_command);
}

//система отладки
if (Telegram\Config::get('debug') === true) {

    require_once('debug/debug.php');

    if (getUserDebug() == $tg->user['id'])
        set_debug($tg, $route, $command_act);
}

if (isset($route)) {

    $result = isset($route['class']) && $route['class'] === true ? new $route['controller']($tg) : new $route['controller']();
    $method = $route['method'];

    //если отправил текст который совпадает с роутом кнопки - то не отрабатываем
    if (isset($route['callback']) && $route['callback'] == true && $tg->callback_status === false)
        exit();

    //врубаем миделверы для данного маршрута
    if (isset($route['middleware'])) {
        foreach ($route['middleware'] as $class_middleware) {
            if (!$class_middleware::next($tg))
                exit();
        }
    }

    if ( method_exists($result, $method) ) {
        $result->setTelegram($tg);
        $result->$method();
    }
}
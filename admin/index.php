<?php


use Telegram\Api\Bot;


session_start();

    require_once '../vendor/autoload.php';
    require_once '../settings/config.php';
    require 'app/config.php';



    // подгружаем настройки
    $settings = require __DIR__ . '/app/settings.php';

    // Создание объекта slim
    $app = new \Slim\App($settings);

    // Инициализация контейнера
    $container = $app->getContainer();

    // Подключение БД
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection([
        'driver' => DRIVER,
        'host' => HOST,
        'database' => DB,
        'username' => USERNAME,
        'password' => PASSWORD,
        'charset' => CHARSET,
        'collation' => COLLATION,
        'prefix' => PREFIX
    ]);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();



    // Определим БД в контейнере
    $container['db'] = function ($container) use ($capsule) {
        return $capsule;
    };

    if (isset($_COOKIE['bot_id']))
        Bot::create($_COOKIE['bot_id']);

    // Регистрируем twig
    $container['view'] = function ($container) {
        $view = new \Slim\Views\Twig(__DIR__.TWIG_PATH, [
            'cache' => false
        ]);

        // Создание экземпляра и добавление специального расширения Slim
        $router = $container->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

        return $view;
    };

    // Подключение маршрутов
    $routes = require __DIR__ . '/app/routes.php';
    $routes($app);

    // Запуск slim
    $app->run();
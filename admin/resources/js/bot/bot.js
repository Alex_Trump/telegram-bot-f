
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'data': {},
            'section': 0,
            'count': 0,
            'selectedYear': false,
            'data_bot': {'bot': {}, 'limits': {}, 'qiwi': {'number' : '', 'token': ''}},
        },
        servers: [],
        mixins: [toast],
        methods: {

            getColor(val) {
                if (val >= 0 && val < 50)
                    return 'bg-success';
                else if (val >= 50 && val < 80)
                    return 'bg-orange';
                else if (val >= 80)
                    return 'bg-red';
            },
            updateSection(section) {
                app.section = section;
                if (section == 0)
                    app.getStatisticsUsers();

            },

            updateInfoBot() {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('bot/updateBotInfo', {}).then(function (response) {
                        app.data_bot = response.data;
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },

            updateQiwi() {

                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('bot/updateQiwi', {
                        'data': app.data_bot.bot.settings.qiwi,
                    }).then(function (response) {

                        app.isClick = true;
                        app.loader = false;

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000,);
                        }
                    });
                }
            },

            start: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('bot/start', {}).then(function (response) {
                        app.data_bot = response.data;
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },

            changeRate: () => {
                axios.post('bot/calculate', {
                    'data': app.data_bot.functions,
                }).then(function (response) {
                    app.count = response.data;
                });
            },

            updateRate() {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('bot/update', {
                        'data': app.data_bot.functions,
                    }).then(function (response) {

                        app.isClick = true;
                        app.loader = false;

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000,);
                        }

                        app.start();

                    });
                }
            },

            getStatisticsUsers: () => {
                app.loader = true;

                axios.post('reporting/getStatisticsUsers', {'year': app.selectedYear})
                    .then(function (response) {
                        app.data = response.data;
                        app.selectedYear = response.data.selectedYear;

                        Highcharts.chart('container', {
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: 'Статистика регистраций за ' + app.selectedYear + ' год'
                            },
                            xAxis: {
                                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                            },
                            yAxis: {
                                title: {
                                    text: 'Пользователи'
                                }
                            },
                            plotOptions: {
                                line: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                    enableMouseTracking: false
                                }
                            },
                            series: [{
                                name: 'Новые пользователи',
                                data: app.data.months
                            }]
                        });

                        app.loader = false;
                    });
            }

        }
    });
    app.start();
    app.getStatisticsUsers();
});

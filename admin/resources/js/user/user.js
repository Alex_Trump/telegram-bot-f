
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'isSelected': false,
            'deleteUser': false,
            'limit': 10,
            'statusUser': 0,
            'selectedStatus': 'default',
            'sorter': '_datetime',
            'sorterType': 'DESC',
            'search': '',
            'users': [],
            'paginate': [],
            'userDetail': []
        },
        mixins: [toast],
        methods: {
            onChangeFilters: (page) => {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('user/filters', {
                        selectedStatus: app.selectedStatus,
                        sorterType: app.sorterType,
                        search: app.search,
                        sorter: app.sorter,
                        limit: app.limit,
                        page: page,
                    })
                    .then(function (response) {
                        app.users = response.data.users;
                        app.paginate = response.data.paginate;

                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },
            updateUser: (id, page) => {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('user/updateUser', {
                        selectedStatus: app.selectedStatus,
                        statusUser: app.statusUser,
                        deleteUser: app.deleteUser,
                        sorterType: app.sorterType,
                        search: app.search,
                        sorter: app.sorter,
                        limit: app.limit,
                        page: page,
                        id: id
                    })
                    .then(function (response) {
                        app.users = response.data.users;
                        app.paginate = response.data.paginate;

                        app.goToast(app, 'Уведомление', 'Действие прошло успешно.', 5000);
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },
            reset: (event) => {
                event.preventDefault();

                app.search = '';
                app.sorter = '_datetime';
                app.sorterType = 'DESC';
                app.selectedStatus = 'default';

                app.onChangeFilters(1);
            },
            detailsUser: (user) => {
                if (Object.keys(user).length) {
                    app.userDetail = user;
                    app.isSelected = (user.status < 0) ? true : false;
                    app.statusUser = user.status;
                    app.deleteUser = false;
                } else {
                    app.goToast(app, 'Уведомление', 'Пользователь удален, просмотр невозможен.', 5000);
                }
            },
            detailsUserParent: (user_id) => {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('user/detailsUserParent', {user_id: user_id})
                    .then(function (response) {
                        app.detailsUser(response.data.user);

                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },
            statusUpdate: () => {
                app.isSelected = app.statusUser;
            },
            sorterCol: (columnName, page) => {
                app.sorter = columnName;
                app.sorterType = (app.sorterType === 'DESC') ? 'ASC' : 'DESC';
                app.onChangeFilters(page);
            }
        }
    });

    app.onChangeFilters(1);
});


let app = new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    data: {
        'error': false,
        'isClick': true,
        'loader': false,
        'login': '',
        'password': ''
    },
    methods: {
        loginStart: () => {
            if(app.isClick) {
                app.isClick = false;
                app.loader = true;

                axios.post('auth', {
                    login: app.login,
                    password: app.password
                })
                .then(function (response) {
                    if (response.data.success === 'ok') {
                        window.location.replace("user");
                    } else {
                        app.isClick = true;
                        app.loader = false;
                        app.error = true;
                    }
                });
            }
        }
    }
});
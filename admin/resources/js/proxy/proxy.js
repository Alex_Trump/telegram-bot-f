
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'data': {'proxy_list': {}},
            'typeProxy': 0,
            'checkedIdArr': {},
            'isSelected': false,
            'limit': 10,
            'selectedStatus': 'default',
            'selectedServer': 'default',
            'selectedCountry': 'default',
            'selectedCategory': 'default',
            'sorter': 'ip',
            'sorterType': 'DESC',
            'search': '',
            'paginate': [],
        },
        servers: [],
        mixins: [toast],
        methods: {
            previewFiles: () => {
                app.file = 1;
                app.$refs.labelImg.innerHTML = app.$refs.file.files[0]['name'];
            },
            sorterCol: (columnName, page) => {
                app.sorter = columnName;
                app.sorterType = (app.sorterType === 'DESC') ? 'ASC' : 'DESC';
                app.onChangeFilters(page);
            },
            updateProxy(type) {
                app.typeProxy = type;
                app.onChangeFilters(1);
                app.checkedIdArr = {};
            },
            filterDate: (timestamp) => {
                return new Date(timestamp * 1000).toLocaleString('ru', {year: 'numeric', month: 'numeric', day: 'numeric', timezone: 'UTC'});
            },
            reset: (event) => {
                event.preventDefault();

                app.search = '';
                app.sorter = 'ip';
                app.sorterType = 'DESC';
                app.selectedStatus = 'default';
                app.selectedServer = 'default';
                app.selectedCountry = 'default';
                app.selectedCategory = 'default';
                app.onChangeFilters(1);
            },
            onChangeFilters: (page) => {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('proxy/get', {
                        typeProxy: app.typeProxy,
                        selectedStatus: app.selectedStatus,
                        selectedServer: app.selectedServer,
                        selectedCountry: app.selectedCountry,
                        selectedCategory: app.selectedCategory,
                        sorterType: app.sorterType,
                        search: app.search,
                        sorter: app.sorter,
                        limit: app.limit,
                        page: page,
                    })
                        .then(function (response) {
                            app.data = response.data;
                            app.paginate = response.data.paginate;

                            app.isClick = true;
                            app.loader = false;
                        });
                }
            },
            checkboxClick(e) {
                if (e.target.checked)
                    app.$set(app.checkedIdArr, e.target.value, e.target.value);
                else
                    app.$delete(app.checkedIdArr, e.target.value);


            },

            deleteProxy() {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('proxy/delete', {
                        proxy_list: app.checkedIdArr,
                    }).then(function (response) {
                        app.goToast(app, 'Уведомление', 'Прокси успешно удалены', 5000,);
                        app.isClick = true;
                        app.loader = false;
                        app.checkedIdArr = {};
                        app.onChangeFilters(1);
                    });
                }
            },

            importProxy() {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    const data = new FormData(document.getElementById('importForm'))
                    var imagefile = document.querySelector('#customFile')
                    var form_data = {
                        typeProxy: app.typeProxy,
                    }

                    data.append('file', imagefile.files[0])
                    data.append('data', JSON.stringify(form_data))

                    axios.post('proxy/import', data, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Ошибка', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000,);
                        }

                        app.isClick = true;
                        app.loader = false;
                        app.file = 0;
                        app.onChangeFilters(1);
                    });

                }
            }
        }
    });
    app.onChangeFilters(1);
});

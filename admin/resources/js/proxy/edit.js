
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'proxy_id': window.proxy_id,
            'isClick': true,
            'loader': false,
            'proxy': {'server_id': '', 'country_id': '', 'category_id': '', 'type_private_proxy': '', 'protocol_proxy': '', 'rotations': 0},
            'data': {},
        },
        mixins: [toast],
        methods: {

            start: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('start', {
                        proxy_id: app.proxy_id,
                    }).then(function (response) {
                        app.data = response.data;
                        app.proxy = response.data.proxy;
                        app.isClick = true;
                        app.loader = false;
                    });
                }

            },

             editProxy: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('/clients/avposting/bot_proxy/admin/proxy/add/create', {
                        update: 1,
                        proxy: app.proxy,
                        typeProxy: app.proxy.type_proxy,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Запись успешно изменена', response.data.msg, 5000,);
                            window.location.replace("/clients/avposting/bot_proxy/admin/proxy");
                        }
                        app.isClick = true;
                        app.loader = false;
                    });
                }

            },

        }
    });
    app.start();
});




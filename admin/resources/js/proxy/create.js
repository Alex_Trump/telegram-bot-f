
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'typeProxy': window.typeProxy,
            'isClick': true,
            'loader': false,
            'proxy': {'server_id': '', 'country_id': '', 'category_id': '', 'type_private_proxy': '', 'protocol_proxy': '', 'rotations': 0},
            'data': {},
        },
        mixins: [toast],
        methods: {

            start: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('start', {
                        typeProxy: app.typeProxy,
                    }).then(function (response) {
                        app.data = response.data;
                        app.isClick = true;
                        app.loader = false;
                    });
                }

            },

            createProxy: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('create', {
                        proxy: app.proxy,
                        typeProxy: app.typeProxy,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            window.location.replace("/clients/avposting/bot_proxy/admin/proxy");
                        }
                        app.isClick = true;
                        app.loader = false;
                    });
                }

            },
        }
    });
    app.start();
});




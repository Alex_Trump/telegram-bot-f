
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'loader': false,
            'data': [],
            'selectedYear': false
        },
        methods: {
            getStatisticsUsers: () => {
                app.loader = true;

                axios.post('reporting/getStatisticsUsers', {'year': app.selectedYear})
                .then(function (response) {
                    app.data = response.data;
                    app.selectedYear = response.data.selectedYear;

                    Highcharts.chart('container', {
                        chart: {
                            type: 'line'
                        },
                        title: {
                            text: 'Статистика регистраций за ' + app.selectedYear + ' год'
                        },
                        xAxis: {
                            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                        },
                        yAxis: {
                            title: {
                                text: 'Пользователи'
                            }
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                                enableMouseTracking: false
                            }
                        },
                        series: [{
                            name: 'Новые пользователи',
                            data: app.data.months
                        }]
                    });

                    app.loader = false;
                });
            }
        }
    });

    app.getStatisticsUsers();
});


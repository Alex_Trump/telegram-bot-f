
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'editData': '',
            'editIndex': 0,
            'deleteIndex': 0,
            'data': {'current_list': {}},
            'current': {'type_proxy': 0, 'country_id': '', 'category_id': '', 'private': '', 'protocol': ''}
        },
        servers: [],
        mixins: [toast],
        methods: {
            start: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('start').then(function (response) {
                        app.data = response.data;
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },
            editDataChange: (data, index) => {
                app.editData = data;
                app.editIndex = index;
            },
            createCurrently: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('create',{
                        current: app.current
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {

                            app.current = {'type_proxy': 0, 'country_id': '', 'category_id': '', 'private': '', 'protocol': ''};

                            app.goToast(app, 'Уведомление', 'Настройка успешно создана', 5000,);
                        }

                        app.isClick = true;
                        app.loader = false;
                        app.start();
                    });
                }
            },

            updateCurrently: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('create',{
                        current: app.editData,
                        update: 1,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.isClick = true;
                            app.loader = false;
                            app.start();
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.isClick = true;
                            app.loader = false;
                            app.$set(app.data.current_list, app.editIndex, app.editData);
                            app.goToast(app, 'Уведомление', 'Настройка успешно изменена', 5000,);
                        }

                        app.isClick = true;
                        app.loader = false;
                        app.start();
                    });
                }
            },

            deleteCurrent()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('delete', {
                        id: app.data.current_list[app.deleteIndex].id,
                    }).then(function (response) {
                        app.goToast(app, 'Уведомление', 'Настройка успешно удалена', 5000,);
                        app.$delete(app.data.current_list, app.deleteIndex);
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },
        },

    });
    app.start();
});


let app = new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    data: {
        'loader': false,
        'isClick': true,
        'oneUser': 0,
        'recipient': 0,
        'userId': '',
        'text': ''
    },
    mixins: [toast],
    methods: {
        choiceRecipient: () => {
            if (app.recipient === '0') {
                app.oneUser = false;
                app.userId = '';
            } else {
                app.oneUser = true;
            }
        },
        sendLetter: () => {
            if (app.isClick) {
                app.isClick = false;

                if (app.text === '')
                {
                    app.goToast(app, 'Уведомление', 'Вы не ввели текст сообщения.', 5000);
                    app.isClick = true;
                }
                else if (app.recipient === '1' && app.userId === '')
                {
                    app.goToast(app, 'Уведомление', 'Вы не ввели ID пользователя.', 5000);
                    app.isClick = true;
                }
                else
                {
                    app.loader = true;

                    axios.post('letter/sendLetter', {
                        'text': app.text,
                        'id': app.userId
                    })
                    .then(function (response) {
                        app.goToast(app, 'Уведомление', 'Рассылка прошла успешно', 5000);

                        app.isClick = true;
                        app.loader = false;
                    });
                }
            }
        }
    }
});



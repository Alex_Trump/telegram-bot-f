
let globalApp;
let timer;

let toast = {
    data() {
        return {
            info: false,
            head_info: '',
            text_info: ''
        }
    },
    methods: {
        goToast: (app, header, body, time, error = false) => {
            globalApp = app;
            clearTimeout(timer);

            globalApp.head_info = header;
            globalApp.text_info = body;
            globalApp.info = true;
            globalApp.error_class = error;

            timer = setTimeout(function () {
                globalApp.info = false;
            }, time);
        },
        closeToast: () => {
            clearTimeout(timer);
            globalApp.info = false;
        }
    }
}
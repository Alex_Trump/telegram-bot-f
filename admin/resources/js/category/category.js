
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'servers': [],
            'textPrompt': 'Развернуть редактирование стран и городов',
            'textPrompt1': 'Развернуть редактирование настроек приватности',
            'textPrompt2': 'Развернуть редактирование настроек протоколов',
            'textPrompt3': 'Развернуть редактирование настроек категорий',
            'prompt': false,
            'prompt1': false,
            'prompt2': false,
            'prompt3': false,
            'data': {'countries': {}, 'private': {}, 'protocols': {}, 'categories': {}},
            'country': {'country': '', 'city': '', 'typeProxy': 0, 'mobileOperator': ''},
            'category': {'category': '', 'section': ''},
            'editData': 0,
            'editIndex': 0,
            'deleteIndex': 0
        },
        servers: [],
        mixins: [toast],
        methods: {

            showPrompt: () => {
                app.prompt = !app.prompt;
                app.textPrompt = app.prompt ? 'Скрыть редактирование стран и городов' : 'Развернуть редактирование стран и городов';
            },

            showPrompt1: () => {
                app.prompt1 = !app.prompt1;
                app.textPrompt1 = app.prompt1 ? 'Скрыть редактирование настроек приватности' : 'Развернуть редактирование настроек приватности';
            },

            showPrompt2: () => {
                app.prompt2 = !app.prompt2;
                app.textPrompt2 = app.prompt2 ? 'Скрыть редактирование настроек протоколов' : 'Развернуть редактирование настроек протоколов';
            },

            showPrompt3: () => {
                app.prompt3 = !app.prompt3;
                app.textPrompt3 = app.prompt3 ? 'Скрыть редактирование настроек категорий' : 'Развернуть редактирование настроек категорий';
            },

            start: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;
                    app.country.country = '';
                    app.country.city = '';
                    app.country.typeProxy = 0;
                    app.country.mobileOperator = '';
                    app.category.category = '';
                    app.category.section = '';


                    axios.post('category/start', {}).then(function (response) {
                        app.data = response.data;
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },
            createCategory()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('category/create', {
                        country: app.country,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', 'Запись успешно создана', 5000,);
                        }

                        app.isClick = true;
                        app.loader = false;
                        app.start();
                    });
                }
            },

            createUserCategory()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('userCategory/create', {
                        category: app.category,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', 'Запись успешно создана', 5000,);
                        }

                        app.isClick = true;
                        app.loader = false;
                        app.start();
                    });
                }
            },

            editDataChange: (data, index) => {
                app.editData = data;
                app.editIndex = index;
            },


            editCategory()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('category/edit', {
                        country: app.editData,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.isClick = true;
                            app.loader = false;
                            app.start();
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.isClick = true;
                            app.loader = false;
                            app.$set(app.data.countries, app.editIndex, app.editData);
                            app.goToast(app, 'Уведомление', 'Запись успешно изменена', 5000,);
                        }


                    });
                }
            },

            editUserCategory()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('userCategory/edit', {
                        category: app.editData,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.isClick = true;
                            app.loader = false;
                            app.start();
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.isClick = true;
                            app.loader = false;
                            app.$set(app.data.countries, app.editIndex, app.editData);
                            app.goToast(app, 'Уведомление', 'Запись успешно изменена', 5000,);
                        }


                    });
                }
            },

            deleteUserCategory()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('userCategory/delete', {
                        id: app.data.categories[app.deleteIndex].id,
                    }).then(function (response) {
                        app.goToast(app, 'Уведомление', 'Запись успешно удалена', 5000,);
                        app.$delete(app.data.categories, app.deleteIndex);
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },

            deleteCategory()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('category/delete', {
                        id: app.data.countries[app.deleteIndex].id,
                    }).then(function (response) {
                        app.goToast(app, 'Уведомление', 'Запись успешно удалена', 5000,);
                        app.$delete(app.data.countries, app.deleteIndex);
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },

            savePrivate()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('category/savePrivate', {
                        private: app.data.private,
                    }).then(function (response) {
                        if (response.data.error == true) {
                            app.isClick = true;
                            app.loader = false;
                            app.start();
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', 'Настройки приватности сохранены', 5000,);
                        }

                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },

            saveProtocols()  {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('category/saveProtocols', {
                        protocols: app.data.protocols,
                    }).then(function (response) {
                        if (response.data.error == true) {
                            app.isClick = true;
                            app.loader = false;
                            app.start();
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', 'Настройки протоколов сохранены', 5000,);
                        }

                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },

        }
    });
    app.start();
});

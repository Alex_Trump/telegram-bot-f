
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'prompt': false,
            'isSave': '',
            'textPrompt': 'Посмотреть подсказку',
            'settings': {'bot': 0},
            'settingTextId': '',
            'settingText': '',
            'settingMainId': '',
            'settingMain': '',
            'dataSetting': {}
        },
        mixins: [toast],
        methods: {
            onChangeFilters: () => {

                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('settings/filters')
                    .then(function (response) {
                        app.settings = response.data;
                        app.isClick = true;
                        app.loader = false;
                    });
                }
            },
            updateSettings: () => {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    app.$set(app.dataSetting, app.settingTextId, app.settingText);
                    app.$set(app.dataSetting, app.settingMainId, app.settingMain);

                    if ((app.settingTextId && app.settingText === '') || (app.settingMainId && app.settingMain === '')) {
                        app.isClick = true;
                        app.loader = false;

                        app.goToast(app, 'Уведомление', 'Нельзя сохранять настройку без значения.', 5000);
                    } else {
                        axios.post('settings/updateSettings', {
                            dataSetting: app.dataSetting
                        })
                            .then(function () {
                                if (app.settingText) app.settings.settings_admin[app.settingTextId]['value'] = app.settingText;
                                if (app.settingMain) app.settings.settings_admin[app.settingMainId]['value'] = app.settingMain;

                                app.goToast(app, 'Уведомление', 'Настройки успешно сохранены.', 5000);
                                app.isClick = true;
                                app.loader = false;
                            });
                    }
                }
            },
            showPrompt: () => {
                app.prompt = !app.prompt;
                app.textPrompt = app.prompt ? 'Скрыть подсказку' : 'Посмотреть подсказку';
            },
            nextSettings: () => {
                app.isSave = (app.settingTextId || app.settingMainId) ? 'ok' : '';
                app.settingText = (app.settingTextId !== '') ? app.settings.settings_admin[app.settingTextId]['value'] : '';
                app.settingMain = (app.settingMainId !== '') ? app.settings.settings_admin[app.settingMainId]['value'] : '';
            }
        }
    });

    app.onChangeFilters();
});


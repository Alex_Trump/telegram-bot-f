

window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'server_id': window.server_id,
            'loader': false,
            'server': {},
        },
        server: {},
        mixins: [toast],
        methods: {
            getServers: () => {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('../get', {
                        id: app.server_id
                    }).then(function (response) {
                        app.server = response.data.servers[0];
                        app.isClick = true;
                        app.loader = false;
                    });

                }
            },

            editServer: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('../edit', {
                        server: app.server,
                        id: app.server_id,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            app.goToast(app, 'Уведомление', 'Успешно сохранено', 5000);
                            window.location.replace("../../servers");
                        }
                        app.isClick = true;
                        app.loader = false;
                    });

                }
            },
        }
    });
    app.getServers();
});




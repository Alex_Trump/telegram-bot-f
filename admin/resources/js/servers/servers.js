
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'servers': [],
        },
        servers: [],
        mixins: [toast],
        methods: {
            getServers: () => {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('servers/get').then(function (response) {
                        app.servers = response.data.servers;
                        app.isClick = true;
                        app.loader = false;
                    });

                }
            },

            deleteServer(id)  {
                if (app.isClick) {
                    app.isClick = false;
                    app.loader = true;

                    axios.post('servers/delete', {
                        id: id
                    }).then(function (response) {
                        app.isClick = true;
                        app.loader = false;
                        app.getServers();
                    });

                }
            }

        }
    });
    app.getServers();
});

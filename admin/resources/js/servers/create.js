
window.addEventListener("load", function() {
    let app = new Vue({
        el: '#app',
        delimiters: ['${', '}'],
        data: {
            'isClick': true,
            'loader': false,
            'server': {'name': '', 'description': '', 'ip': '', 'port':'', 'login':'', 'password': '', 'location': ''},
        },
        mixins: [toast],
        methods: {
            createServer: () => {
                if (app.isClick) {

                    app.isClick = false;
                    app.loader = true;

                    axios.post('add/create', {
                        server: app.server,
                    }).then(function (response) {

                        if (response.data.error == true) {
                            app.goToast(app, 'Уведомление', response.data.msg, 5000, true);
                        } else {
                            window.location.replace("../servers");
                        }
                        app.isClick = true;
                        app.loader = false;
                    });
                }

            },
        }
    });

});




<?php

    return [
        'settings' => [
            'debug' => true,
            'displayErrorDetails' => true,
            'cookies.encrypt' => true,
            'cookies.path' => '/',
            'cookies.secret_key' => 'c2MAxVocYQ59UNkKhdIUqWkWyNMDaFmWmAECplVsvQA'
        ]
    ];
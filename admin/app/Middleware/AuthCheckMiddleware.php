<?php

namespace Admin\Middleware;

class authCheckMiddleware extends Middleware
{

    public function __invoke($request, $response, $next)
    {
        if(!$_COOKIE['authorized']) {
            return $response->withRedirect('/clients/avposting/bot_proxy/admin/login');
        }

        $response = $next($request, $response);

        return $response;
    }

}
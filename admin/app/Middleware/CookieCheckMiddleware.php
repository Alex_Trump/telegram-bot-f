<?php

namespace Admin\Middleware;

class CookieCheckMiddleware extends Middleware
{

    public function __invoke($request, $response, $next)
    {
        if(isset($_COOKIE['authorized']) && $_COOKIE['authorized']) {
            return $response->withRedirect('/clients/avposting/bot_proxy/admin/user');
        }

        $response = $next($request, $response);

        return $response;
    }

}
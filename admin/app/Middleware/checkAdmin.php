<?php


namespace Admin\Middleware;


class checkAdmin extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if(!isset($_COOKIE['admin']) || $_COOKIE['admin'] != 1)  {
            return $response->withRedirect('/clients/avposting/bot_proxy/admin/login');
        }

        $response = $next($request, $response);

        return $response;
    }
}
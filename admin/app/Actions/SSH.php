<?php

namespace Admin\Actions;

use phpseclib\Net\SSH2;

class SSH
{

    static $ssh = null;

    public static function testConnect($ip, $port, $login, $password)
    {
        return self::connect($ip, $port, $login, $password);
    }

    public static function connect($ip, $port, $login, $password)
    {
        self::$ssh = new SSH2($ip.':'.$port);
        return self::$ssh->login($login, $password);
    }

    public static function exec($exec)
    {
        return self::$ssh->exec($exec);
    }

}
<?php


namespace Admin\Actions;

use App\Helper\Helper;

class Validations
{

    public static $errors = [];

    public static function checkQiwiApi($number, $token, $name)
    {
        $out  = Helper::requestQiwi("https://edge.qiwi.com/payment-history/v2/persons/".$number."/payments", $token);

        if (!$out)
            self::$errors[] = "Неверные данные <b>".$name."</b>";

    }

    public static function checkMin($value, $min, $name)
    {
        if (!isset($value) || $value < $min)
            self::$errors[] = "Значение <b>".$name."</b> должно быть больше ".$value;

        return true;
    }

    public static function checkMax($value, $max, $name)
    {
        if (!isset($value) || $value > $max)
            self::$errors[] = "Значение <b>".$name."</b> должно быть меньше ".$value;

        return true;
    }

    public static function checkNumeric($value, $name)
    {
        if (!isset($value) || !is_int($value))
            self::$errors[] = "Значение <b>".$name."</b> должно быть целым числом";

        return true;
    }

    public static function checkRequire($value, $name)
    {
        if (!isset($value) || $value == '')
            self::$errors[] = "Значение <b>".$name."</b> не может быть пустым";

        return true;
    }

    public static function checkValueMas($val, $mas, $name)
    {
        if (!in_array($val, $mas, true))
            self::$errors[] = "Неверное значение в <b>".$name."</b>";

        return true;
    }


    public static function issetError()
    {
        return count(self::$errors) > 0;
    }

    public static function getErrorMessage()
    {
        $msg = "";

        foreach (self::$errors as $error) {
            $msg .= $error."<br><br>";
        }

        return $msg;
    }

}
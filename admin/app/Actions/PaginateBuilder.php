<?php

namespace Admin\Actions;

class PaginateBuilder
{

    public static function paginate($page, $limit , $sql, $object, &$data)
    {
        // Получаем кол-во элементов и кол-во страниц
        $count = $sql->get()->count();
        $countPages = ceil($count / $limit);

        // Формируем (стартовое/конечное) значение пагинации
        if ($page > 3) {
            $endPages = $page + 2;

            $data['paginate']['endPages'] = ($endPages > $countPages) ? $countPages : $endPages;
            $data['paginate']['startPages'] = $page - 2;

            if (($page + 1) == $countPages) {
                $data['paginate']['startPages'] = $page - 3;
            } elseif ($page >= $countPages) {
                $page = ($page > $countPages) ? $countPages : $page;
                $data['paginate']['startPages'] = $page - 4;
            }
        } else {
            $data['paginate']['endPages'] = ($countPages < 5) ? $countPages : 5;
            $data['paginate']['startPages'] = 1;
        }

        // Формируем следующие и предыдущее значение пагинации
        $data['paginate']['pageNext'] = ($page == $countPages) ? false : $page + 1;
        $data['paginate']['pageBack'] = $page - 1;

        // Запоминаем текущее значение пагинации и кол-во страниц
        $data['paginate']['pageActive'] = $page;
        $data['paginate']['countPages'] = $countPages;
        $data['paginate']['countEl'] = $count;

        $skip = ($page * $limit) - $limit;
        $result = $sql->take($limit)->skip($skip)->get();

        $data[$object] = count($result) ? $result : 0;
    }

}
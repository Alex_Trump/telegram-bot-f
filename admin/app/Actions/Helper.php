<?php


namespace Admin\Actions;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Telegram\Settings;

class Helper
{

    public static function percent($value_sum, $z)
    {
        return round(($z / $value_sum) * 100);
    }

    public static function calculate($user, $post)
    {
        $functions = $user->settings->settings['rate']['functions'];

        $count = 0;

        foreach ($post->data as $key => $value) {
            if ($value && !$functions[$key])
                $count += Settings::get($key);
        }

        return $count;
    }

    public static function generateReport(array $errors)
    {
        $link = URL_SITE.'/report';
        file_put_contents('upload/'.$_COOKIE['user_id'].'/'.$_COOKIE['user_id'].md5($_COOKIE['user_id'].'secret').'.txt', json_encode($errors));
        return $link;
    }


    public static function parseCsv(string $url, $type_proxy = 0)
    {

        $arr = [];
        $oSpreadsheet = IOFactory::load($url);
        $oCells = $oSpreadsheet->getActiveSheet()->getCellCollection();

        for ($iRow = 1; $iRow <= $oCells->getHighestRow(); $iRow++)
        {

            $r = [];

            if ($iRow == 1)
                continue;


            for ($iCol = 'A'; $iCol <= $oCells->getHighestColumn(); $iCol++)
            {
                if ($type_proxy == 0 && $iCol == 'M')
                    break;

                if ($type_proxy == 1 && $iCol == 'P')
                    break;


                $oCell = $oCells->get($iCol.$iRow);
                $r[]  = $oCell ? $oCell->getValue() : null;

            }

            if ($type_proxy == 0 && count($r) < 12) {
                break;
            }

            if ($type_proxy == 1 && count($r) < 15) {
                break;
            }

            $arr[] = $r;
        }

        if (count($arr) == 0)
            return ['error' => true, 'msg' => 'Неверное количество столбцов в файле'];

        return ['error' => false, 'data' => $arr];
    }


    public static function getNameTypeProxy($name = null, $status = false)
    {
        $arr = [
            'shared' => 'Шаред',
            'private' => 'Приватный',
            'pprivate' => 'Полуприватный',
            'ipv4_https' => 'IPV4 HTTPS',
            'ipv4_socks5' => 'IPV4 SOCKS5',
            'ipv4_https_socks5' => 'IPV4 HTTPS/SOCKS5',
            'ipv6_https' => 'IPV6 HTTPS',
            'ipv6_socks5' => 'IPV6 SOCKS5',
            'ipv6_https_socks5' => 'IPV6 HTTPS/SOCKS5',
        ];

        if ($status) {
            $key = array_search($name, $arr);
            return $key ?? $arr;
        } else
            return $arr[$name] ?? $arr;
    }

    public static function getArrayTypeProxy($json) :array
    {
        $return_arr = [];

        foreach ($json as $key => $arr) {
            if ($arr)
                $return_arr[] = ['value' => $key, 'name' => self::getNameTypeProxy($key)];
        }

        return $return_arr;
    }
}
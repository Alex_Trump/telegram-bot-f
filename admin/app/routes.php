<?php

use Slim\App;

return function (App $app) {

    $container = $app->getContainer();

    /* START Авторизация START */
        $app->get('/', function ($request, $response) {
             return $response->withRedirect('/clients/avposting/bot_proxy/admin/login');
        });

        $app->get('/login', \Admin\Controllers\LoginController::class . ':index')
            ->add(new \Admin\Middleware\CookieCheckMiddleware($container));

        $app->post('/auth', \Admin\Controllers\LoginController::class . ':auth')
            ->add(new \Admin\Middleware\CookieCheckMiddleware($container));
    /* END Авторизация END */


 


    /* START Страница пользователей START */
        $app->get('/user', \Admin\Controllers\Home\UserController::class . ':index')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));

        $app->post('/user/filters', \Admin\Controllers\Home\UserController::class . ':filters')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));

        $app->post('/user/updateUser', \Admin\Controllers\Home\UserController::class . ':updateUser')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));

        $app->post('/user/detailsUserParent', \Admin\Controllers\Home\UserController::class . ':detailsUserParent')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));

        $app->get('/exit', function ($request, $response) {
            setcookie('authorized', false);
            return $response->withRedirect('/clients/avposting/bot_proxy/admin/login');
        });
    /* END Страница пользователей END */

    $app->get('/report', \Admin\Controllers\Home\UserController::class . ':report')
        ->add(new \Admin\Middleware\AuthCheckMiddleware($container));


 

    /* START Страница настроек START */
        $app->get('/settings', \Admin\Controllers\Home\SettingsController::class . ':index')
            ->add(new \Admin\Middleware\checkAdmin($container));
        $app->post('/settings/filters', \Admin\Controllers\Home\SettingsController::class . ':filters')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));
        $app->post('/settings/updateSettings', \Admin\Controllers\Home\SettingsController::class . ':updateSettings')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));
    /* END Страница пользователей END */

    /* START Страница отчетности START */
        $app->post('/reporting/getStatisticsUsers', \Admin\Controllers\Home\ReportingController::class . ':getStatisticsUsers')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));
    /* END Страница пользователей END */

    /* START Страница рассылки START */
        $app->post('/letter/sendLetter', \Admin\Controllers\Home\LetterController::class . ':sendLetter')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));

        $app->get('/letter', \Admin\Controllers\Home\LetterController::class . ':index')
            ->add(new \Admin\Middleware\AuthCheckMiddleware($container));
    /* END Страница рассылки END */

};
<?php

namespace Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{

    protected $table = 'users';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'user_id',
        'username',
        'first_name',
        'last_name',
        'is_bot',
        '_datetime',
        'number',
        'str_date',
        'parent',
        'command',
        'command_data',
        'status',
        'active'
    ];

}
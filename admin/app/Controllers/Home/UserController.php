<?php

namespace Admin\Controllers\Home;

use Admin\Controllers\MainController;
use App\Model\User as Users;
use Admin\Actions\PaginateBuilder;
use Illuminate\Database\Capsule\Manager as DB;

class UserController extends MainController
{

    public function index($request, $response)
    {

        $this->data['title'] = 'Пользователи';
        $this->data['isAuth'] = true;
        $this->data['bot_id'] = $_COOKIE['bot_id'];
        $this->data['admin'] = $_COOKIE['admin'];

        return $this->view->render($response, 'home/user.twig', [
            'data' => $this->data
        ]);
    }

    public function report($request, $response)
    {

        $this->data['title'] = 'Последний отчет об ошибках';
        $this->data['isAuth'] = true;
        $this->data['bot_id'] = $_COOKIE['bot_id'];
        $this->data['admin'] = $_COOKIE['admin'];

        $errors = json_decode(file_get_contents(URL_SITE.'/upload/'.$_COOKIE['user_id'].'/'.$_COOKIE['user_id'].md5($_COOKIE['user_id'].'secret').'.txt'), true);

        $this->data['errors'] = $errors;

        return $this->view->render($response, 'home/report.twig', [
            'data' => $this->data
        ]);
    }

    public function filters($request, $response, $filters = false)
    {

        $postData = $filters ? $filters : json_decode($request->getBody()->getContents());

        if (in_array($postData->selectedStatus, ['all', 'default'])) {
            $sql = Users::whereIn('status', [-1, 0, 1]);
        } else {
            $sql = Users::where('status', $postData->selectedStatus);
        }

        if ($postData->search) {
            $sql->where(function ($query) use ($postData) {
                $query->where('username', 'like', '%' . trim($postData->search) . '%')
                    ->orWhere('user_id', 'like', '%' . trim($postData->search) . '%');
            });
        }

        $sql->orderBy($postData->sorter, $postData->sorterType);
        $sql->groupBy('user_id');

        PaginateBuilder::paginate($postData->page, $postData->limit, $sql, 'users', $this->data);

        return $response->withJson($this->data);
    }

    public function updateUser($request, $response)
    {
        $postData = json_decode($request->getBody()->getContents());

        if ($postData->deleteUser) {
            Users::where('user_id', $postData->id)->delete();
        } else {
            $postData->statusUser = $postData->statusUser ? -1 : 0;
            Users::where('user_id', $postData->id)->update(['status' => $postData->statusUser]);
        }

        return $this->filters($request, $response, $postData);
    }

    public function detailsUserParent($request, $response)
    {
        $postData = json_decode($request->getBody()->getContents());

        $user = DB::table('users as u1')
            ->select('u1.*', DB::raw('COUNT(u2.parent) AS count_parent'))
            ->join('users as u2', 'u1.user_id', '=', 'u2.parent', 'left')
            ->where('u1.user_id',  $postData->user_id)
            ->groupBy('u1.user_id')
            ->limit(1)->get()->toArray();

        $this->data['user'] = $user ? $user[0] : [];

        return $response->withJson($this->data);
    }

}
<?php

namespace Admin\Controllers;

class MainController
{

    protected $container;
    protected $data = [];

    public function __construct($container)
    {
        $this->container = $container;
        $this->data['url'] = URL_SITE;
    }

    public function __get($property)
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }
    }

}
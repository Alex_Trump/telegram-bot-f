<?php

namespace Admin\Controllers;

use App\Model\Bot;

class LoginController extends MainController
{

    public function index($request, $response)
    {
        $this->data['title'] = 'Вход в админку';

        return $this->view->render($response, 'login/login.twig', [
            'data' => $this->data
        ]);
    }

    public function auth($request, $response)
    {
        $posData = json_decode($request->getBody()->getContents());

        if ($posData->login === LOGIN_USER && $posData->password === PASSWORD_USER) {
            setcookie("authorized", true);
            setcookie("bot_id", 0);
            setcookie("user_id", 15);
            setcookie("admin", 1);
            $data = ['success' => 'ok'];
        } elseif ($bot = Bot::where(['username' => $posData->login, 'password' => $posData->password])->first()) {
            setcookie("authorized", true);
            setcookie("bot_id", $bot->bot_id);
            setcookie("user_id", $bot->user_id);
            setcookie("admin", 0);
            $data = ['success' => 'ok'];
        } else
            $data = ['success' => 'error'];


        return $newResponse = $response->withJson($data);
    }

}
<?php


namespace App;

use App\Model\User;
use Telegram\Api\Send;

class Controller
{

    protected $tg;
    protected $user_id;
    protected $message_id;
    protected $user;

    public function setTelegram($tg)
    {
        $this->tg = $tg;
        $this->user_id = $tg->user['id'];
        $this->message_id = $tg->message['message_id'] ?? null;
        $this->checkBlock();
    }

    public function checkBlock()
    {
        $this->user = User::where('user_id', $this->user_id)->first();

        if ($this->user->status < 0) {
            Send::message($this->user_id, 'Ваш аккаунт заблокирован');
            exit();
        }
    }

}
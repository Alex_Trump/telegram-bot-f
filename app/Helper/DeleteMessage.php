<?php
namespace App\Helper;

use Illuminate\Database\Capsule\Manager as DB;
use Telegram\Api\Send;

class DeleteMessage
{

    public static function deleteMessage($user_id, $message_id = 0, $delete_id = 0)
    {

        if ($message_id > 0)
            $messages = DB::table('message_delete')->where(['user_id' => $user_id, 'message_id' => $message_id])->get();
        elseif ($delete_id > 0)
            $messages = DB::table('message_delete')->where(['user_id' => $user_id, 'delete_id' => $delete_id])->get();
        else
            $messages = DB::table('message_delete')->where(['user_id' => $user_id, 'delete_id' => 0])->get();


        $ids = [];

        foreach ($messages as $message) {
            $ids[] = $message->id;
            Send::deleteMessage($user_id, $message->message_id);
        }

        DB::table('message_delete')->whereIn('id', $ids)->delete();

    }

    public static function setMessage($user_id, $message_id, $delete_id = 0)
    {
        DB::table('debugs')->insert(
            ['user_id' => $user_id, 'message_id' => $message_id, 'delete_id' => $delete_id]
        );
    }

}
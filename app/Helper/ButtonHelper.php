<?php

namespace App\Helper;

use Telegram\Api\Button;

class ButtonHelper
{

    public static function getButton($key, $status = 'keyboard') {

        $button = new Button($status);

        switch ($key) {

            case 'menu':
                $button->set('1');
                $button->set('2', true);
                $button->set('3', true);
                $button->set('3', true);
                break;

        }

        return  $button->count() == 0 ? null : $button->get();

    }

}

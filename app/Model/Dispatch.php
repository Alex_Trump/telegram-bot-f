<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dispatch extends Model {

    protected $table = "dispatch_list";

    protected $casts = [
        'users' => 'array',
        'errors' => 'array',
    ];

    protected $fillable = array(
        "users", 'msg', 'errors',
        'str_datetime', 'photo', 'url',
        'count_users', 'count_send',
        'count_click', 'status', 'created_at', 'update_at'
    );

}
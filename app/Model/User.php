<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    protected $table = "users";
    public $timestamps = false;
    protected $fillable = array("user_id", 'first_name', 'last_name', 'username', 'is_bot', 'str_date', 'parrent', 'command', 'command_data', 'status');

}
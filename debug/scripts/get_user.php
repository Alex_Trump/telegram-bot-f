<?php

require ('init.php');


Telegram\DB::connect(); //подключаем базу

use Illuminate\Database\Capsule\Manager as DB;

if (isset($_GET['user_id'])) {

    $user = DB::table('users')->where('user_id', $_GET['user_id'])->first();

    if ($user) {

        $mas = ['ok' => true, 'user' => ['user_id' => $user->user_id, 'name' => $user->first_name, 'username' => $user->username]];

        file_put_contents('../user_id.txt', $user->user_id);

    } else {

        $mas = ['ok' => false, 'text' => 'Пользователь не найден'];

        file_put_contents('../user_id.txt', "");
    }

    DB::table('debugs')->delete();

    echo json_encode($mas, true);

}
<?php

require ('init.php');

Telegram\DB::connect(); //подключаем базу

use Illuminate\Database\Capsule\Manager as DB;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


if (isset($_GET['last'])) {

    $modal = [];

    $users = json_decode(DB::table('debugs')->where('id', '>', $_GET['last'])->get()->toJson(), true);

    if ($users) {

        $msg = "";


        foreach ($users as $user) {

            $data = json_decode($user['data'], true);
            $params = json_decode($user['params'], true);
            $route = json_decode($user['route'], true);
            $tg = json_decode($user['tg'], true);

            $msg .= "<tr>";

            if ($user['error'] == 1)
                $msg .= "<td class='table-danger'><b>Ошибка</b></td>";
            else
                $msg .= "<td class='table-success'><b>Успешно</b></td>";

            $msg .= "<td>";
            if ($params !== null) {
                foreach ($params as $param) {
                    $msg .= is_array($param['value']) ? "<b>".$param['name']."</b>: <pre>".print_r($param['value'], true)."</pre>" : "<b>".$param['name']."</b>: ".$param['value'];
                    $msg .= "<br>";
                }
            }
            $msg .= "</td>";

            $msg .= "<td>";

            if ($user['command_act'] !== null)
                $msg .= '<b>Активная команда:</b> '.$user['command_act'].'<br><br>';

            if ($route !== null) {
                $msg .= "<b>Контроллер:</b> ".$route['controller'];
                $msg .= "<br>";
                $msg .= "<b>Метод:</b> ".$route['method'];
                $msg .= "<br>";
                $msg .= isset($route['callback']) ? "<b>Callback:</b> 1" : '';
                $msg .= "<br>";
                $msg .= isset($route['middleware']) ? "<b>Middleware:</b> ".implode(' - ', $route['middleware']) : '';
                $msg .= "<br>";
                $msg .= isset($route['no_act']) ? "<b>Всегда активная:</b> 1" : '';
                $msg .= "<br>";
                $msg .= "<br>";

            } else
                $msg .= "Отсутсвует";
            $msg .= "</td>";


            $msg .= "<td>";
            if ($data !== null) {
                foreach ($data as $key => $d) {
                    $d_id = rand(1, 9999999);
                    $msg .= "<b>Действие:</b> ".$d['name'];
                    $msg .= "<br>";
                    $msg .= "<b>Входящие данные:</b> <pre>".print_r($d['data'], true)."</pre>";
                    $msg .= "<br>";
                    $msg .= $d['result']['ok'] === true ? "<b>Статус:</b> <span class='text-success'>Успешно</span>" : "<b>Статус:</b> <span class='text-danger'>Ошибка</span>";
                    $msg .= "<br><br>";
                    $msg .= '<button type="button" class="btn btn-info m" data-toggle="modal" data-m="'.$d_id.'" data-target="modal1">Посмотреть ответ</button>';
                    $msg .= "<br>";
                    $msg .= "<br>";

                    $modal[$d_id] = "<pre>".print_r($d['result'], true)."</pre>";
                }
            } else
                $msg .= "Действия не найдены";
            $msg .= "</td>";
            $d_1 = rand(1, 9999999);
            $msg .= '<td><button type="button" class="btn btn-info m" data-m="'.$d_1.'" data-target="modal1">Открыть объект</button></td>';

            $modal[$d_1] = "<pre>".print_r($tg, true)."</pre>";;


            $msg .= "</tr>";

            $last = $user['id'];
        }

        $mas = ['ok' => true, 'last' => $last, 'html' => $msg, 'modal' => $modal];

        echo json_encode($mas, true);
    }



}

<?php

use Illuminate\Database\Capsule\Manager as DB;

$id_debug = 0;

function getUserDebug()
{
    return file_get_contents('debug/user_id.txt');
}

function dd($msg = null)
{
    global $id_debug;

    if ($id_debug > 0 && isset($msg)) {

        $trace = debug_backtrace();
        $vLine = file( $trace[0]['file']);
        $fLine = $vLine[ $trace[0]['line'] - 1 ];
        preg_match( "#\\$(\w+)#", $fLine, $match );

        set_param($match[0], $msg);
    }
}

function set_param($name, $value)
{
    global $id_debug;

    $debug = DB::table('debugs')->find($id_debug);
    $params = $debug->params !== null ? json_decode($debug->params, true) : [];
    $params[] = ['name' => $name, 'value' => $value];

    DB::table('debugs')
        ->where('id', $id_debug)
        ->update(['params' => json_encode($params)]);

}

function set_method($method, $data, $out)
{
    global $id_debug;

    $debug = DB::table('debugs')->find($id_debug);

    $error = 0;

    if ($debug->error == 0)
        $error = $out['ok'] === true ? 0 : 1;

    $params = $debug->data !== null ? json_decode($debug->data, true) : [];
    $params[] = ['name' => $method, 'data' => $data, 'result' => $out];

    DB::table('debugs')
        ->where('id', $debug->id)
        ->update(['error' => $error, 'data' => json_encode($params)]);
}

function set_debug($tg, $route, $c_a = null)
{
    global $id_debug;

    $id_debug = DB::table('debugs')->insertGetId(
        ['tg' => json_encode($tg), 'route' => json_encode($route), 'command_act' => $c_a]
    );

    define("DEBUG", true);
}


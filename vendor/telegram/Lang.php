<?php


namespace Telegram;


class Lang
{

    public $lang;
    public $text;
    public $params;

    public static function get($name, $params = [], $lang_name = null)
    {

        $lang = new self();
        $lang->params = $params;
        $lang->lang = $lang_name ?? Config::get('lang');

        return $lang->getText($name);
    }


    private function getText($name)
    {
        $data = require "lang/".$this->lang.".php";

        if (isset($data[$name]))
            return is_array($this->params) && count($this->params) > 0 ? $this->decor($data[$name]) : $data[$name];

        return null;
    }

    private function decor($text)
    {

        foreach ($this->params as $key => $param) {
            $text = str_replace(':'.$key, $param, $text);
        }

        return $text;
    }

}
<?php

namespace Telegram\Api;

use Telegram\Config;

class File
{

    public static function savePhoto($chat_id, $file)
    {

        $file = array_pop($file);

        $result = Curl::request('getFile', ['file_id' => $file['file_id']]);

        mkdir("file/".$chat_id, 0777);

        if (isset($result['file_path'])) {

            $file_save_path = "file/".$chat_id."/".$chat_id . "_" . time().".jpg";
            self::save($file_save_path, $result['file_path']);

            return $file_save_path;
        }

        return null;
    }

    public static function saveDocument($chat_id, $file)
    {

        $result = Curl::request('getFile', ['file_id' => $file['file_id']]);

        mkdir("file/".$chat_id, 0777);

        if (isset($result['file_path'])) {

            $mas_url = explode('/', $result['file_path']);
            $file_save_path = "file/".$chat_id."/".$chat_id . "_" . time().'_'.array_pop($mas_url);

            self::save($file_save_path, $result['file_path']);

            return $file_save_path;
        }

        return null;
    }

    public static function save($url, $file_path)
    {
        $file = "https://api.telegram.org/file/bot" . Config::get('api_key') . "/" . $file_path;
        file_put_contents($url, file_get_contents($file));
    }

}
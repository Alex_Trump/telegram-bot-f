<?php

namespace Telegram\Api;

class Send {

    public static function editMessage($chat_id, $text, $message_id, $button = []) {

        $method = "editMessageText";

        $data = [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'text' => $text,
            'parse_mode' => 'html',
        ];

        if ($button)
            $data['reply_markup'] = $button;


        return self::send($method, $data);
    }

    public static function editMessageButton($chat_id, $message_id, $button) {

        $method = "editMessageReplyMarkup";

        $data = [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'parse_mode' => 'html',
            'reply_markup' => $button
        ];

        return self::send($method, $data);
    }

    public static function editMessageCaption($chat_id, $text, $message_id, $button = []) {

        $method = "editMessageCaption";

        $data = [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'caption' => $text,
            'parse_mode' => 'html',
        ];

        if ($button)
            $data['reply_markup'] = $button;

        return self::send($method, $data);
    }

    public static function deleteMessage($chat_id, $message_id) {

        $method = "deleteMessage";

        $data = [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
        ];

        return self::send($method, $data);
    }

    public static function message($chat_id, $text, $button = []) {

        $method = "sendMessage";

        $data = [
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'html',
        ];

        if ($button)
            $data['reply_markup'] = $button;

        return self::send($method, $data);
    }

    public static function photo($chat_id, $url_photo, $caption = '', $button = []) {

        $method = "sendPhoto";

        $data = [
            'chat_id' => $chat_id,
            'caption' => $caption,
            'parse_mode' => 'html',
            'photo' => $url_photo,
        ];

        if ($button)
            $data['reply_markup'] = $button;

        return self::send($method, $data);
    }

    public static function document($chat_id, $url_document, $caption = '', $button = []) {

        $method = "sendDocument";

        $data = [
            'chat_id' => $chat_id,
            'caption' => $caption,
            'parse_mode' => 'html',
            'document' => $url_document,
        ];

        if ($button)
            $data['reply_markup'] = $button;

        return self::send($method, $data);
    }

    public static function sendMediaGroup($chat_id, $media, $button = []) {

        $method = "sendMediaGroup";

        $data = [
            'chat_id' => $chat_id,
            'media' => json_encode($media),
        ];

        if ($button)
            $data['reply_markup'] = $button;


        return self::send($method, $data);
    }


    public static function answer($callback_query_id, $text, $show_alert = false) {

        $method = "answerCallbackQuery";

        $data = [
            'callback_query_id' => $callback_query_id,
            'text' => $text,
            'show_alert' => $show_alert,
        ];

        return self::send($method, $data);
    }


    public static function send($method, $data)
    {
        return Curl::request($method, $data);
    }




}

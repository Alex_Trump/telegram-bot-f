<?php

namespace Telegram\Api;

class Button {

    private $status;
    private $button = [];
    private $coll = [];

    public $resize_keyboard = true;
    public $one_time_keyboard = false;

    public function __construct($status = null)
    {
        if (isset($status))
            $this->status = $status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function set($name, $break = false)
    {

        $this->coll[] = ['text' => $name];

        if ($break) {
            $this->button[] = $this->coll;
            $this->coll = [];
        }

    }

    public function setInline($name, $route, $data = null, $break = false)
    {

        $params = '';

        if (isset($data))
            $params = is_array($data) ? '/@/'.implode('/@/', $data) : '/@/'.$data;

        $msg = $route.$params;

        $this->coll[] = ['text' => $name, 'callback_data' => $msg];

        if ($break) {
            $this->button[] = $this->coll;
            $this->coll = [];
        }

    }

    public function setUrl($name, $url, $break = false)
    {
        $this->coll[] = ['text' => $name, 'url' => $url];

        if ($break) {
            $this->button[] = $this->coll;
            $this->coll = [];
        }

    }

    public function clear()
    {
        $this->coll = [];
        $this->button = [];
    }

    public function button_set_coll()
    {
        if ($this->coll) {
            $this->button[] = $this->coll;
            $this->coll = [];
        }
    }

    public function get()
    {

        $this->button_set_coll();

        $data = [
            'one_time_keyboard' => $this->one_time_keyboard,
            'resize_keyboard' => $this->resize_keyboard,
            $this->status => $this->button
        ];

        return json_encode($data);
    }

    public function getRotate($button, $count)
    {
        $button_new = [];
        $r = [];
        $s = [];

        foreach ($button as $item) {
            $s = array_merge($s, $item);
        }

        foreach($s as $k => $b) {

            $r[] = $b;

            if (count($r) == $count || count($s) == ($k + 1)) {
                $button_new[] = $r;
                $r = [];
            }
        }

        return $button_new;

    }

    public function setRotate($num)
    {
        $this->button_set_coll();
        $this->button = $this->getRotate($this->button, $num);
    }

    public function count()
    {
        $this->button_set_coll();
        return count($this->button);
    }
}

<?php

namespace Telegram\Api;

use Telegram\Config;

class Curl {

    public static function request ($method, $data, $api_key = null)
    {

        if (!isset($api_key))
            $api_key = Config::get('api_key');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot'.$api_key.'/' . $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST'); //Отправляем через POST
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //Сами данные отправляемые

        $out = json_decode(curl_exec($curl), true); //Получаем результат выполнения, который сразу расшифровываем из JSON'a в массив для удобства
        curl_close($curl); //Закрываем курл

        if (defined('DEBUG') && DEBUG === true)
            set_method($method, $data, $out);


        return $out['ok'] === true ? $out['result'] : $out; //Отправляем ответ в виде массива


    }
}


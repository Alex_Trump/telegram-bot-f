<?php

namespace Telegram\Api;

use App\Model\User;

class Sequence {


    public static function getCommandAct($chat_id)
    {
        $user = User::where('user_id', $chat_id)->first();
        
        return $user ? $user->command : null;
    }

    public static function getCommandData($chat_id)
    {
        $user = User::where('user_id', $chat_id)->first();

        return $user ? $user->command_data : null;
    }

    public static function setCommandAct($chat_id, $data, $command_data = null)
    {
        $user = User::where('user_id', $chat_id)->first();

        if ($user) {
            $user->command = $data;
            $user->command_data = $command_data;
            $user->save();
        }
    }

    public static function setCommandActNull($chat_id)
    {
        $user = User::where('user_id', $chat_id)->first();

        if ($user) {
            $user->command = null;
            $user->command_data = null;
            $user->save();
        }

    }

}

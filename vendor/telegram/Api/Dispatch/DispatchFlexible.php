<?php

namespace Telegram\Api\Dispatch;

use App\Model\Dispatch;

class DispatchFlexible
{
    public $photo = null;
    public $url = null;
    public $datetime = 0;
    public $users = array();
    public $msg = "";

    public static function create(array $users, string $msg = "") :object
    {
        $self = new Self;
        $self->users = $users;
        $self->msg = $msg;
        return $self;
    }


    public function date(int $date) :object
    {
        $this->datetime = $date;
        return $this;
    }

    public function photo(string $file_id) :object
    {
        $this->photo = $file_id;
        return $this;
    }

    public function url(string $url) :object
    {
        $this->url = $url;
        return $this;
    }

    public function send()
    {
        Dispatch::create([
            'users' => $this->users,
            'msg' => $this->msg,
            'str_datetime' => $this->datetime,
            'photo' => $this->photo,
            'url' => $this->url,
            'count_users' => count($this->users),
        ]);

        return true;
    }

}
<?php


namespace Telegram\Api\Dispatch;

use App\Model\Dispatch;
use App\Model\User;
use Telegram\Api\Send;
use Telegram\Api\Button;

class DispatchSend
{

    private $users = [];
    private $msg = null;
    private $photo = null;
    private $url = null;
    private $button = [];
    private $errors = [];

    private static function checkPermission()
    {
        return User::where('status', 1)->get()->count('id') == 0 ? true : false;
    }

    public static function check()
    {
        if (static::checkPermission()) {

            $dispatch = Dispatch::where([
                ['status', '=', 0],
                ['str_datetime', '<=', time()]
            ])->first();

            if ($dispatch) {

                $send = new static($dispatch);
                return $send->send();

            } else
                return "Новых рассылок нет";
        } else
            return "Рассылка уже запущена";


    }


    public function __construct(object $dispatch)
    {
        $this->dispatch = $dispatch;
        $this->users = $dispatch->users;
        $this->msg = $dispatch->msg;
        $this->url = $dispatch->url;
        $this->photo = $dispatch->photo;

        if ($this->url !== null)
            $this->button = $this->buttonCreate();

    }

    public function send()
    {
        foreach ($this->users as $user)
            $this->photo !== null ? $this->sendPhoto($user) : $this->sendMessage($user);

        return $this->save();
    }

    private function buttonCreate()
    {
        $button = new Button('inline_keyboard');
        $button->setUrl('Перейти', $this->url);
        return $button->get();
    }

    private function checkResult(array $result, int $user)
    {
        if (!isset($result['error_code'])) {
            $this->dispatch->count_send++;
            return true;
        } else {
            $this->errors[] = ['user' => $user, 'error' => $result['description']];
            return false;
        }

    }

    private function sendPhoto(int $user)
    {
        return $this->checkResult(Send::photo($user, $this->photo, $this->msg, $this->button), $user);
    }

    private function sendMessage(int $user)
    {
        return $this->checkResult(Send::message($user, $this->msg, $this->button), $user);
    }


    private function save()
    {
        if ($this->errors)
            $this->dispatch->errors = $this->errors;

        $this->dispatch->status = 2;

        return $this->dispatch->save();

    }


}
<?php


namespace Telegram;

use Illuminate\Database\Capsule\Manager as DB;

class Settings
{

    public static function set($name, $value)
    {
        DB::table('settings')
            ->where('name', $name)
            ->update(['value' => $value]);
    }

    public static function get($name)
    {
        $settings = DB::table('settings')->where('name', $name)->first();
        return $settings->value;
    }

}
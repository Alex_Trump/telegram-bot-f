<?php

namespace Telegram;

use Illuminate\Database\Capsule\Manager as Capsule;

class DB
{
    public static function connect()
    {
        $capsule = new Capsule;
        $capsule->addConnection([
            "driver" => 'mysql',
            "host" => Config::get('db.host'),
            "database" => Config::get('db.name'),
            "username" => Config::get('db.user'),
            "password" => Config::get('db.password'),
            "charset" => "utf8",
            "collation" => "utf8_unicode_ci",
            "prefix" => "",
        ]);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

}


<?php

namespace Telegram;

use App\Model\User;

class Start
{
    public static function startOperations($tg)
    {
        $user = User::where('user_id', $tg->user['id'])->first();

        if (!$user) {

            $tg->new = true;

            self::setUser($tg->user['id'], [
                'parent' => $tg->parent,
                'username' => $tg->user['username'] ?? '',
                'last_name' => $tg->user['last_name'] ?? '',
                'first_name' => $tg->user['first_name'] ?? '',
            ]);
        } else {
            self::updateUser($user, [
                'username' => $tg->user['username'] ?? '',
                'last_name' => $tg->user['last_name'] ?? '',
                'first_name' => $tg->user['first_name'] ?? '',
            ]);
        }

    }

    public static function setUser($chat_id, $data) {
        User::create([
            'user_id' => $chat_id,
            'parent' => $data['parent'],
            'username' => (string) $data['username'],
            'first_name' => (string) $data['first_name'],
            'last_name' => $data['last_name'],
            'str_date' => time(),
            'status' => 0
        ]);
    }

    public static function updateUser($user, $data) {
        $user->username = $data['username'];
        $user->first_name = (string)$data['first_name'];
        $user->last_name = (string)$data['last_name'];
        $user->command = null;
        $user->command_data = null;
        $user->save();
    }

}

?>

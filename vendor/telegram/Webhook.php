<?php

namespace Telegram;

class Webhook
{
    /**
     * Data from Telegram
     *
     * @var array
     */
    private $telegram;

    /**
     * Data from inline Button
     *
     * @var array
     */
    private $callback_data;

    /**
     * The User from Telegram
     *
     * @var array
     */
    public $user;


    /**
     * The status of new user
     *
     * @var array
     */
    public $new = false;

    /**
     * Id of the inviter
     *
     * @var int
     */
    public $parent = 0;

    /**
     * The Message from Telegram
     *
     * @var array
     */
    public $message;

    /**
     * The Status callback
     *
     * @var bool
     */
    public $callback_status = false;

    /**
     * The Route name
     *
     * @var bool
     */
    public $name_command;


    /**
     * Create a new event instance.
     *
     * @param  array  $tg
     * @return void
     */
    public function __construct($tg)
    {

        $this->telegram = $tg;

        if (isset($this->telegram['callback_query']['id']))
            $this->callbackForm($this->telegram);
        else
            $this->messageForm($this->telegram);

    }

    /**
     * Filling properties when we are inline
     *
     * @param  array  $tg
     * @return void
     */
    private function callbackForm($tg)
    {
        $this->user = $tg['callback_query']['from'];
        $this->callback_status = true;
        $this->message = $tg['callback_query']['message'];
        $this->name_command = $this->getCallbackNameCommand($tg['callback_query']['data']);
        $this->callback_data = $this->getCallbackDataCommand($tg['callback_query']['data']);
    }

    /**
     * Return callback_query_id
     *
     * @param  array  $tg
     * @return string
     */
    public function getCallbackQueryId()
    {
        return $this->telegram['callback_query']['id'];
    }

    /**
     * Filling properties when we are message
     *
     * @param  array  $tg
     * @return void
     */
    private function messageForm($tg)
    {
        $this->user = $tg['message']['from'];
        $this->message = $tg['message'];
        $this->name_command = $this->deleteSimbString(($tg['message']['text'] ?? ''));
    }


    /**
     * From inline data we get the first
     *
     * @param  string $data
     * @return string
     */
    private function getCallbackNameCommand(string $data)
    {
        $arr = explode('/@/', $data);
        return $this->deleteSimbString($arr[0]);
    }

    /**
     * From string delete all smile
     *
     * @param  string $str
     * @return string
     */
    private function deleteSimbString($str)
    {
        $new_str = trim(trim(preg_replace('/[\x{1000}-\x{10FFFF}]/u', "\xEF\xBF\xBD", $str), '�'));
        return $new_str == "" ? $str : $new_str;
    }

    /**
     * From inline data we delete the first
     *
     * @param  string $data
     * @return array
     */
    private function getCallbackDataCommand(string $data = '')
    {
        $arr = explode('/@/', $data);
        unset($arr[0]);
        return array_values($arr);
    }


    public function getCallbackData($key = 0)
    {
        return $this->callback_data[$key] ?? null;
    }


    public function checkStart()
    {
        if (preg_match("/^\/start/", ($this->message['text'] ?? ''))) {
            $this->parent = (int)str_replace("/start", "", $this->message['text']);
            $this->name_command = "/start";
            return true;
        }

        return false;

    }
}

?>
